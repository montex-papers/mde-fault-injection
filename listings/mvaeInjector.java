public class MVAEAction implements InjectionAction {

  @Override
  public boolean doAction(ASTNode root) {
    ASTNode target = getTarget(root);
    if(target != null) {
      return new DeleteAction().doAction(target);
    }
    return false;
  }
	
  private ASTNode getTarget(ASTNode root) {
    if (root instanceof Assignment) {
      Assignment assignment = (Assignment) root;
      return assignment.getRightHandSide();
    }
    return null;
  }
}
