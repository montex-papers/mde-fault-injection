#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.2 patchlevel 6    last modified 2019-01-01 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2018
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
set terminal svg size 900,400 enhanced font 'Arial,12' 
set output 'time.svg'

set datafile separator ";"
set style data histogram
set style histogram cluster gap 3
set style fill solid border -1

set grid
set logscale y
set yrange [100:25000]
set ytics (100,250,500,1000,2500,5000,10000,25000,50000)

set xrange [-0.5:12.5]

set xlabel "Fault type"
set ylabel "Total injection time (milliseconds, logarithmic scale)" offset 1,0

set key top left horizontal outside maxrows 1 maxcols 6 samplen 2

plot 'time.csv'	u ($2):xtic(1) t 'gdx-ai',\
				'' u ($3) t 'Malmo',\
				'' u ($4) t 'Tink',\
				'' u ($5) t 'Zuul',\
				'' u ($6) t 'Cryptomator',\
				'' u ($7) t 'Spring OAuth'

set terminal pdf size 9,4 enhanced
set output 'time.pdf'
replot

set output 'time-patterns.pdf'
set style fill pattern border -1
replot
